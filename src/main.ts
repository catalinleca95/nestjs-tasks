import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  // NODE_ENV - to get current env
  const serverConfig = config.get('server'); // getting default yaml 'server'
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || serverConfig.port;

  await app.listen(port);
}
bootstrap();
