import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from './user.entity';

// data - data provided to the decorator
// Whatever we return from here will be set to the decorated param
export const GetUser = createParamDecorator(
  (data, ctx: ExecutionContext): User => {
    const req = ctx.switchToHttp().getRequest();
    return req.user;
  },
);
